# This is homework project for implementing extended ASCII to UTF-8 conversion.

## Building
You'll need to have cmake >= 3.5 installed and added to your path.
Open your terminal in root of this repository and run the following code:

```bash
mkdir build
cd build
cmake ..
```
Built project files will be placed in "build" folder.
Note: If you are on macOS you might want to run cmake .. -G"Xcode" to generate Xcode project.  

### Remember - building in release mode will yield the best performance.  

## Entities

### encoding
This library implements Unicode character encoding to UTF-8 function, as well as encoding table utility class.

### transcoder
This executable acts as a filter that transforms single byte encoded text to UTF-8 (receives data on STDIN, and outputs to STDOUT).  
Allows CLI user interaction.  

Usage:  
```bash
cat ./texts/386intel.txt | ./utf-8-transcoder -e ./tables/CP437_1.TXT > output.txt
```

Help:
```bash
./utf-8-transcoder -h
```
Dependencies: ```encoding```.  

 
