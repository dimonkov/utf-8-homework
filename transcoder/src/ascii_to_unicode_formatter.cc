#include <iostream>
#include "encoding/encoding.h"
#include "ascii_to_unicode_formatter.h"

namespace fmt
{
	AsciiToUnicodeFormatter::AsciiToUnicodeFormatter(encoding::EncodingTable& table, bool bom) : table(table), bom(bom)
	{
	}
	void AsciiToUnicodeFormatter::Format(std::istream & in, std::ostream & out) const
	{
		static char buf[4];
		int data_length = -1;
		static const size_t read_buf_size = 8192;
		static char read_buffer[read_buf_size];

		//Print BOM in the beggining
		if (bom)
		{
			for (int i = 0; i < encoding::UTF_8_BOM.size(); i++)
			{
				out << encoding::UTF_8_BOM[i];
			}
		}

		in >> std::noskipws;
		int good = 0;
		while (in.read(read_buffer, sizeof(read_buffer)))
		{
			good = in.gcount();
			for (int j = 0; j < good; j++)
			{
				int unicode_char_code = table.GetCharacterTranslation(read_buffer[j]);

				encoding::UnicodeToUtf_8(buf, unicode_char_code, data_length);

				for (int i = 0; i < data_length; i++)
				{
					out << buf[i];
				}
			}
		}
	}
}