#include <iostream>
#include <fstream>
#include "encoding/encoding.h"
#include "encoding/encoding_table.h"
#include "ascii_to_unicode_formatter.h"
#include "cli_parameters.h"


int main(int argc, char** argv)
{
	CliParameters param = CliParameters::ParseCliParameters(argc, argv);

	std::ios_base::sync_with_stdio(false);

	std::ifstream encoding_table_stream;
	encoding_table_stream.open(param.encoding_table_file_name.c_str());

	if (!encoding_table_stream.is_open())
	{
		std::cerr << "Cannot open the encoding file!!!" << param.encoding_table_file_name << std::endl;
		return 1;
	}

	encoding::EncodingTable table = encoding::EncodingTable::ParseEncodingTableFromStream(encoding_table_stream);

	fmt::AsciiToUnicodeFormatter formatter(table, param.use_bom);

	formatter.Format(std::cin, std::cout);

	return 0;
}