#include <string>
#include <iostream>
#include <stdlib.h>
#include "cli_parameters.h"

CliParameters::CliParameters() : use_bom(true) { }

CliParameters CliParameters::ParseCliParameters(int argc, char ** argv)
{
	bool encoding_table_file_specified = false;
	CliParameters param;

	for (int i = 0; i < argc; i++)
	{
		std::string arg = std::string(argv[i]);
		if (arg == "-e" && i+1 < argc)
		{
			param.encoding_table_file_name = std::string(argv[i + 1]);
			encoding_table_file_specified = true;
		}
		if (arg == "-h")
		{
			std::cout << std::endl << "Options:" << std::endl;
			std::cout << "\"-h\" Prints this screen" << std::endl;
			std::cout << "\"-e\" <filename> Allows to specify the encoding table file name" << std::endl;
			std::cout << "\"-b\" Disables BOM for UTF-8 (enabled by default)" << std::endl;
			exit(EXIT_SUCCESS);
		}
		if (arg == "-b")
		{
			param.use_bom = false;
		}
	}

	if (!encoding_table_file_specified)
	{
		std::cerr << "No encoding table specified, please use -e <filename>!" << std::endl;
		exit(EXIT_FAILURE);
	}

	return param;
}
