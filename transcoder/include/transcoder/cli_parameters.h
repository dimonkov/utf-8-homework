#ifndef CLI_PARAMETERS_H_
#define CLI_PARAMETERS_H_
#include <string>

class CliParameters
{
public:
	std::string encoding_table_file_name;
	bool use_bom;

	CliParameters();

	static CliParameters ParseCliParameters(int argc, char** argv);
};

#endif // !CLI_PARAMETERS_H_
