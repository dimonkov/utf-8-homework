#ifndef ASCII_TO_UNICODE_FORMATTER_H_
#define ASCII_TO_UNICODE_FORMATTER_H_
#include "encoding/encoding_table.h"
#include "formatter.h"


namespace fmt
{
	class AsciiToUnicodeFormatter : public Formatter
	{
	public:
		AsciiToUnicodeFormatter(encoding::EncodingTable& table, bool bom = true);
		virtual void Format(std::istream& in, std::ostream& out) const override;

	private:
		encoding::EncodingTable& table;
		bool bom;
	};

}

#endif // !ASCII_TO_UNICODE_FORMATTER_H_
