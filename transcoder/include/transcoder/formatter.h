#ifndef FORMATTER_H_
#define FORMATTER_H_

#include <iostream>

namespace fmt
{
	class Formatter
	{
	public:
		virtual void Format(std::istream& in, std::ostream& out) const = 0;

		virtual ~Formatter();
	};
}


#endif // !FORMATTER_H_
