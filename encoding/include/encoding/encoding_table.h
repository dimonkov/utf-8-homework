#ifndef ENCODING_TABLE_H
#define ENCODING_TABLE_H
#include <iostream>

namespace encoding
{
	class EncodingTable
	{
	public: 
		void AddCharacterCodeTranslation(char ascii_code, unsigned int unicode_code);
		unsigned int GetCharacterTranslation(char ascii_code) const;


		static EncodingTable ParseEncodingTableFromStream(std::istream& stream);
	private:
		unsigned int character_map[256];
	};
}

#endif // !ENCODING_TABLE_H
