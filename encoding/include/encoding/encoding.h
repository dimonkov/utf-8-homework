#ifndef ENCODING_H_
#define ENCODING_H_
#include <array>

namespace encoding
{
	const std::array<char, 3> UTF_8_BOM{ { (char)0xEF, (char)0xBB, (char)0xBF } };
	const char EXTRA_BYTE_PATTERN = 0b10000000;
	const char EXTRA_BYTE_MASK = 0b00111111;

	void UnicodeToUtf_8(char buf[4], unsigned int unicode, int& dataLength);
}

#endif // !ENCODING_H_
