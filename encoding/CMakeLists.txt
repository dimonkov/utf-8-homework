project(encoding)

# compile library
add_library(encoding STATIC 

include/encoding/encoding.h
src/encoding.cc

include/encoding/encoding_table.h
src/encoding_table.cc

)

#Include own headers, expose headers for the others
target_include_directories(encoding INTERFACE include PRIVATE include/encoding)