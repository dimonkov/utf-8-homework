#include "encoding.h"

namespace encoding
{
	void UnicodeToUtf_8(char buf[4], unsigned int unicode, int& dataLength)
	{
		//Simple ASCII, nothing fancy
		if (unicode < 128)
		{
			buf[0] = static_cast<char>(unicode);
			dataLength = 1;
			return;
		}
		//Get the byte size of the UTF-8 character
		dataLength = unicode < 2048 ? 2 :
			unicode < 65536 ? 3 : 4;

		//Fill all the least significant bytes first
		for (int byte = dataLength - 1; byte > 0; byte--)
		{
			buf[byte] = EXTRA_BYTE_PATTERN | (unicode & EXTRA_BYTE_MASK);
			unicode = unicode >> 6;
		}

		//Prepare the template that defines the continuation lenth
		char first_byte_templ = ((~0) << (8 - dataLength));

		//Write all the bits that didn't fit into continuation
		buf[0] = first_byte_templ | unicode;
	}
}
