#include <exception>
#include <string>
#include <sstream>
#include "encoding_table.h"


namespace encoding
{
	void EncodingTable::AddCharacterCodeTranslation(char ascii_code, unsigned int unicode_code)
	{
		character_map[(unsigned char)ascii_code] = unicode_code;
	}
	unsigned int EncodingTable::GetCharacterTranslation(char ascii_code) const
	{
		return character_map[(unsigned char)ascii_code];
	}
	EncodingTable EncodingTable::ParseEncodingTableFromStream(std::istream& stream)
	{
		//Return empty table if can't parse file
		if (stream.eof())
		{
			std::cerr << "Cannot parse encoding table!!!" << std::endl;

			throw new std::runtime_error("Cannot parse encoding table!!!");
		}
		int ascii;
		unsigned int unicode;
		std::string line;

		EncodingTable table;
		
		while (std::getline(stream, line))
		{
			size_t find_pos = line.find_first_of('#');

			if (find_pos == 0)
				continue;

			std::istringstream ss(line);

			ss >> std::hex >> ascii >> unicode;

			if (stream)
			{
				table.AddCharacterCodeTranslation(ascii, unicode);
			}
		}

		return table;
	}
}